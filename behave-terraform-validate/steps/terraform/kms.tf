resource "aws_kms_key" "mykmskey" {
  description             = "This key is used to encrypt all resources"
  deletion_window_in_days = 10
}
