#!/usr/bin/python
from behave import *
import terraform_validate
import os

resource_name = {
    "dynamodb": "aws_dynamodb_table"
}

encryption_property = {
    "aws_dynamodb_table": "server_side_encryption"
}

@given('I have terraform template at "{terraform}" directory')
def step_impl(context, terraform):
    print (terraform)
    Path = os.path.join(os.path.dirname(os.path.realpath(__file__)), terraform)
    print (Path)
    context.response = terraform_validate.Validator(Path)
    print (context.response)

@when('I define a "{service}" in terraform template')
def step_impl(context, service):
    if(service in resource_name.keys()):
        resource = resource_name[service]
    print (resource)
    context.resource_type = resource
    print (context.resource_type)
    context.resources = context.response.resources(resource)
    print (context.resources)

@then('encryption must be enabled')
def step_impl(context):
    context.response.error_if_property_missing()
    prop = encryption_property[context.resource_type]
    print (prop)
    context.resources.should_have_properties(prop)
