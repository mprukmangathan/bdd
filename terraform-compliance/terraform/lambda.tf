resource "aws_lambda_function" "test_lambda" {
  filename      = "lambda_function_payload.zip"
  function_name = "lambda_function_name"
  role          = aws_iam_role.iam_for_lambda.arn
  handler       = "exports.test"

  depends_on = [aws_iam_role_policy_attachment.lambda_logs, aws_cloudwatch_log_group.cw_log_group]

  # The filebase64sha256() function is available in Terraform 0.11.12 and later
  # For Terraform 0.11.11 and earlier, use the base64sha256() function and the file() function:
  # source_code_hash = "${base64sha256(file("lambda_function_payload.zip"))}"
  source_code_hash = filebase64sha256("lambda_function_payload.zip")

  runtime = "nodejs12.x"

  vpc_config {
    subnet_ids = ["subnet-34bf725f", "subnet-d598fb99", "subnet-fa0a3180"]
    security_group_ids = ["sg-087c330f3f4038f4f"]
  }

  environment {
    variables = {
      S3_BUCKET = "mytest_lambda_bucket"
    }
  }

  kms_key_arn = aws_kms_key.mykmskey.arn

  tags = var.lambda_tags
}
