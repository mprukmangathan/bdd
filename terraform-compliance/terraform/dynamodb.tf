resource "aws_dynamodb_table" "config_aggregator" {
  name           = "cmdb-config-aggregator"
  billing_mode   = "PROVISIONED"
  read_capacity  = var.read_capacity
  write_capacity = var.write_capacity
  hash_key       = "accountid"
  range_key      = "date"

  attribute {
    name = "accountid"
    type = "N"
  }

  attribute {
    name = "date"
    type = "S"
  }
 
  server_side_encryption {
    enabled = true
    kms_key_arn = aws_kms_key.mykmskey.arn
  }

  tags = var.dynamodb_tags
}
