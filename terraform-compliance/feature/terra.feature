Feature: Evaluating dynamodb and lambda with citi standard

# Validating Encryption
  Scenario: Evaluating Encryption for dynamodb
        Given I have aws_dynamodb_table defined
        Then it must contain server_side_encryption

# Validating tags
  Scenario Outline: Ensure that specific tags are defined
        Given I have aws_dynamodb_table defined
        #When it contains tags
        Then it must contain tags
        And its value must not be null
		
		Examples:
			| tags         |
			| Name         |
			| Environment  |

# Validating Encryption
  Scenario: Ensure Encryption for lambda environments
        Given I have aws_lambda_function defined
        When it has environment
        Then it must contain kms_key_arn

# Validating VPC for Lambda
  Scenario: Evaluating VPC for Lambda
        Given I have aws_lambda_function defined
        Then it must contain vpc_config
        And its value must not be null
        #When its property does not include subnet_ids security_group_ids
        #Then it fails

# Validating tags
  Scenario Outline: Ensure tags are defined
        Given I have aws_lambda_function defined
        Then it must contain tags
        And its value must not be null
        Examples:
                | tags        |
                | Name        |
                | Environment |

# Validate IAM Policy
  Scenario: Reject if a policy allows exclusive permissions
        Given I have aws_iam_policy defined
        When it contains policy
        And it contains Statement
        And its Effect is Allow
        And it contains Resource
        Then its value must not match the "\*" regex
