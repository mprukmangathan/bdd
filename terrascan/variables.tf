variable "bucket_name" {
  description = "Name of s3 bucket"
}

variable "public" {
  description = "boolean to add public access to s3 bucket. Defaults to private"
  default     = false
}

variable "force_destroy" {
  description = "force destroy bucket"
}

variable "bucket_policy" {
  description = "Path to s3 bucket policy"
}

variable "versioning" {
  description = "boolean to enable verisioning on bucket or not"
}

variable "tags" {
  description = "map of tags to add to s3 bucket"
  type        = map(string)
}

variable "cartid_tag" {
  description = "mandatory citi tag"
  default     = 0
}

variable "softwareaudit_tag" {
  description = "mandatory citi tag"
  default     = " "
}

variable "billingprofileid_tag" {
  description = "mandatory citi tag"
  default     = 0
}

variable "citisystemsinventory_tag" {
  description = "mandatory citi tag"
  default     = 0
}

variable "Environment" {
  description = "mandatory citi tag"
  default     = " "
}

variable "SectorSpecificGOC" {
  description = "mandatory citi tag"
  default     = " "
}
